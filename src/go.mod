module codeberg.org/readeck/readeck

go 1.21

require (
	github.com/CloudyKit/jet/v6 v6.2.0
	github.com/antchfx/htmlquery v1.3.0
	github.com/antchfx/xpath v1.2.4 // indirect
	github.com/anthonynsimon/bild v0.13.0
	github.com/araddon/dateparse v0.0.0-20210429162001-6b43995a97de
	github.com/casbin/casbin/v2 v2.73.1
	github.com/cristalhq/jwt/v3 v3.1.0
	github.com/dop251/goja v0.0.0-20230806174421-c933cf95e127
	github.com/doug-martin/goqu/v9 v9.18.0
	github.com/fatih/color v1.15.0
	github.com/go-chi/chi/v5 v5.0.10
	github.com/go-redis/redis/v8 v8.11.5
	github.com/go-shiori/dom v0.0.0-20230515143342-73569d674e1c
	github.com/go-shiori/go-readability v0.0.0-20230421032831-c66949dfc0ad
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/google/uuid v1.3.0
	github.com/gorilla/csrf v1.7.1
	github.com/gorilla/securecookie v1.1.1
	github.com/hlandau/passlib v1.0.11
	github.com/jackc/pgx/v4 v4.18.1
	github.com/jarcoal/httpmock v1.3.0
	github.com/kinbiko/jsonassert v1.1.1
	github.com/komkom/toml v0.1.2
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/mattn/go-sqlite3 v1.14.17
	github.com/sirupsen/logrus v1.9.3
	github.com/spyzhov/ajson v0.9.0
	github.com/stretchr/testify v1.8.4
	github.com/tdewolff/parse/v2 v2.6.7
	github.com/xhit/go-simple-mail/v2 v2.15.0
	golang.org/x/image v0.11.0
	golang.org/x/net v0.14.0
	golang.org/x/sync v0.3.0
	golang.org/x/sys v0.11.0 // indirect
	golang.org/x/text v0.12.0
	gopkg.in/hlandau/easymetric.v1 v1.0.0 // indirect
	gopkg.in/hlandau/measurable.v1 v1.0.1 // indirect
	gopkg.in/hlandau/passlib.v1 v1.0.11
)

require (
	github.com/JohannesKaufmann/html-to-markdown v1.4.0
	github.com/antchfx/xmlquery v1.3.17
	github.com/cristalhq/acmd v0.11.1
	github.com/gabriel-vasile/mimetype v1.4.2
	github.com/halorium/env v1.0.0
	github.com/lithammer/shortuuid/v4 v4.0.0
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
	golang.org/x/term v0.11.0
	modernc.org/sqlite v1.25.0
)

require (
	github.com/CloudyKit/fastprinter v0.0.0-20200109182630-33d98a066a53 // indirect
	github.com/Knetic/govaluate v3.0.1-0.20171022003610-9aa49832a739+incompatible // indirect
	github.com/PuerkitoBio/goquery v1.8.1 // indirect
	github.com/andybalholm/cascadia v1.3.2 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/dlclark/regexp2 v1.10.0 // indirect
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/go-sourcemap/sourcemap v2.1.3+incompatible // indirect
	github.com/go-test/deep v1.1.0 // indirect
	github.com/gogs/chardet v0.0.0-20211120154057-b7413eaefb8f // indirect
	github.com/google/pprof v0.0.0-20230808223545-4887780b67fb // indirect
	github.com/jackc/chunkreader/v2 v2.0.1 // indirect
	github.com/jackc/pgconn v1.14.1 // indirect
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgproto3/v2 v2.3.2 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/pgtype v1.14.0 // indirect
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	github.com/tidwall/gjson v1.16.0 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.1 // indirect
	github.com/toorop/go-dkim v0.0.0-20201103131630-e1cd1a0a5208 // indirect
	golang.org/x/crypto v0.12.0 // indirect
	golang.org/x/mod v0.8.0 // indirect
	golang.org/x/tools v0.6.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	lukechampine.com/uint128 v1.2.0 // indirect
	modernc.org/cc/v3 v3.40.0 // indirect
	modernc.org/ccgo/v3 v3.16.13 // indirect
	modernc.org/libc v1.24.1 // indirect
	modernc.org/mathutil v1.5.0 // indirect
	modernc.org/memory v1.6.0 // indirect
	modernc.org/opt v0.1.3 // indirect
	modernc.org/strutil v1.1.3 // indirect
	modernc.org/token v1.0.1 // indirect
)
