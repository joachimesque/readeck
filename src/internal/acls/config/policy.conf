# SPDX-FileCopyrightText: © 2021 Olivier Meunier <olivier@neokraft.net>
#
# SPDX-License-Identifier: AGPL-3.0-only

# -------------------------------------------------------------------
# Roles
# -------------------------------------------------------------------

# System routes
p, /system/read,    system, read


# Admin
p, /api/admin/read,     api:admin:users,    read
p, /api/admin/write,    api:admin:users,    write
p, /web/admin/read,     admin:users,        read
p, /web/admin/write,    admin:users,        write


# Cookbook
p, /api/cookbook/read,  api:cookbook,   read
p, /web/cookbook/read,  cookbook,       read


# User profile
p, /api/profile/read,   api:profile,    read
p, /api/profile/write,  api:profile,    write
p, /web/profile/read,   profile,        read
p, /web/profile/write,  profile,        write

# Credentials (app passwords)
p, /web/profile/credentials/read,    profile:credentials, read
p, /web/profile/credentials/write,   profile:credentials, write

# API Tokens
p, /web/profile/tokens/read,    profile:tokens, read
p, /web/profile/tokens/write,   profile:tokens, write


# Bookmarks
p, /api/bookmarks/read,     api:bookmarks,  read
p, /api/bookmarks/write,    api:bookmarks,  write
p, /api/bookmarks/export,   api:bookmarks,  export
p, /web/bookmarks/read,     bookmarks,      read
p, /web/bookmarks/write,    bookmarks,      write

# Bookmark collections
p, /api/bookmarks/collections/read,     api:bookmarks:collections,  read
p, /api/bookmarks/collections/write,    api:bookmarks:collections,  write
p, /web/bookmarks/collections/read,     bookmarks:collections,      read
p, /web/bookmarks/collections/write,    bookmarks:collections,      write


# OPDS catalog
p, /api/opds/read,  api:opds,   read


# -------------------------------------------------------------------
# Groups
# -------------------------------------------------------------------

# Group "user"
g, user, /*/profile/*
g, user, /*/profile/credentials/*
g, user, /*/bookmarks/read
g, user, /*/bookmarks/write
g, user, /*/bookmarks/export
g, user, /*/bookmarks/collections/read
g, user, /*/bookmarks/collections/write
g, user, /api/opds/*

# Group "staff"
g, staff, user
g, staff, /system/*
g, staff, /*/profile/tokens/*

# Group "admin"
g, admin, staff
g, admin, /*/admin/*
g, admin, /*/cookbook/*


# -------------------------------------------------------------------
# Scoped roles (used for token and application passwords)
# -------------------------------------------------------------------

# Bookmarks read only
g, scoped_bookmarks_r, /api/bookmarks/read
g, scoped_bookmarks_r, /api/bookmarks/export
g, scoped_bookmarks_r, /api/bookmarks/collections/read
g, scoped_bookmarks_r, /api/opds/read
g, scoped_bookmarks_r, /api/profile/read

# Bookmarks write only
g, scoped_bookmarks_w, /api/bookmarks/write
g, scoped_bookmarks_w, /api/bookmarks/collections/write
g, scoped_bookmarks_w, /api/profile/read

# Admin read only
g, scoped_admin_r, /api/admin/read
g, scoped_admin_r, /api/profile/read

# Admin write only
g, scoped_admin_w, /api/admin/write
g, scoped_admin_w, /api/profile/read
