# Bookmarks

Bookmarks are where you save the web content you like.

## Create a new Bookmark

Found a web page you like? Great! Copy its link into the text field named **new bookmark** on the [Bookmark List](readeck-instance://bookmarks).

![New Bookmark form](img/bookmark-new.png)

After a few seconds, your bookmark will be ready. You can then open it to read or watch its content, add labels, highlight text or export an ebook (for more information, please read the [Bookmark View](./bookmark.md) section).

## Bookmark type

Readeck recognizes 3 different types of web content:

### Article

An article is a page from which the text content was extracted. It renders as a readable version of its content.

### Picture

A picture is a page that was recognized as a picture container (ie. a link to Unsplash). It renders the stored picture.

### Video

A video is a page that was identified as a video container (ie. a link to Youtube or Vimeo). It renders a video player. Please note that videos are played from their respective remote servers.


## Bookmark List

The [bookmark list](readeck-instance://bookmarks) is where you'll find all your saved bookmarks.

### Navigation

On the sidebar, you'll find a search field and links that will take you to filtered bookmark lists.

![Bookmark list sidebar](./img/bookmark-sidebar.png)

- **Search** \
  Enter any search term (title, content, website...)
- **All** \
  All your bookmarks.
- **Unread** \
  The bookmarks that are not in the archive.
- **Archive** \
  The bookmarks you marked as archived.
- **Favorites** \
  The bookmarks you marked as favorite.


Once you start saving pages, you'll see the following additional links:

- **Articles** \
  Your article bookmarks
- **Videos** \
  Your video bookmarks
- **Pictures** \
  Your picture bookmarks

Finally, you'll see 3 more sections that take you to bookmark related pages:

- **[Labels](./labels.md)** \
  All your bookmark labels
- **Highligts** \
  All the highlights created on your bookmarks
- **[Collections](./collections.md)** \
  The list of all your collections

### Bookmark Cards

Each item on a list is called a Bookmark Card.

![Bookmark Card Interface](./img/bookmark-card.png)

A card shows:

- the **title** on which you can click to watch or read the bookmark,
- the **site name**,
- the estimated **reading time**,
- the **tag list**,
- **action buttons**

The action buttons perform the following:

- **Favorite** \
  This toggles the favorite status of the bookmark.
- **Archive** \
  This moves the bookmark to the archives (or removes it from there).
- **Delete** \
  This marks the bookmark for deletion (it can be canceled during a few seconds).


## Filter results

On the bookmark list, you can filter your results based on one or several criteria. Click on the button "Filter list" next to the page title to open the filtering form.

![Bookmark list filters](./img/bookmark-filters.png)
The filter form

Enter any criteria and click on **Save**.

Please note that if you want to search with an exact text, you must quote the text.\
For example, if you want to filter by a label **fluffy cat**, enter **"fluffy cat"** in the **Label** field.

The "site" field lets you search by the website name or its domain.

After you performed a search, you can save it into a new [collection](./collections.md) to make it permanent.
