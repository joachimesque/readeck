# User Profile

The [Profile section](readeck-instance://profile) of Readeck lets you change your personal information, you password and some other settings.

## Edit your profile

On the main profile page, you can change your username and your email address.

## Change your password

On the [Password](readeck-instance://profile/password) page, you can change the password you use to connect to Readeck.

## Application Passwords

If you need to grant access to your Readeck account to a service or an app, you can't provide you main username and password; it won't work.

What you can do is create an [Application Password](readeck-instance://profile/credentials).

Once you created an application password, you can use it to access the Readeck API or export services (not yet documented).

You can limit what a given password can access through the API.

See the [Ebook Catalog](./opds.md) help page for a real life example.
