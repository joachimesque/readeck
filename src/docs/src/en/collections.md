# Collections

Collections let you easily organize and export your bookmarks
by saving search requests of your choice.
When you create a new bookmark that matches a collection's criteria,
it will appear immediately.
Here are some examples:

- The unread articles (no photos or videos),
- The archived articles from "wikipedia.org"
- The pictures with the tag "cat",
- The articles that contain "breaking" in their title

## Create a new collection

To create a new collection, go to the [Collection List](readeck-instance://bookmarks/collections) and click on **Create a new collection**.

![New Collection form](img/collection-new.png)
The collection form

Enter any criteria and click on **Save**.

Please note that if you want to search with an exact text, you must quote the text.\
For example, if you want to filter by a label **fluffy cat**, enter **"fluffy cat"** in the **Label** field.

Filters like label or author can contain multiple values, separated by a space.


## Export a collection

On a collection page, you can export the full collection to a file.

For now, only EPUB is available and exports the full collection as a single book.


## Delete a collection

On a collection page, open the **Edit** box and click on **Delete**.

This operation can be cancelled during a few seconds, in case you made a mistake.
