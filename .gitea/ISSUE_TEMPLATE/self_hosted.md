---
name: Self Hosted
about: When you encounter problems with self-hosting Readeck 🤓
labels:
  - bug
  - 'self hosted'
---

## What happened?

(describe the steps that led to the bug you encountered)

## What outcome was expected?



## Screenshots

<details>
<summary>Screenshots</summary>

- (if you have screenshots, please paste them here. If not, you can delete from `<details>` to `</detail>`)

</details>

## Technical stuff (logs, etc)

<details>
<summary>Logs</summary>

```
(If you have logs to provide, please paste them here. If not, you can delete from `<details>` to `</detail>`)
```

</details>

