---
name: Bug Report
about: When something goes wrong and it doesn’t fit the other categories 😣
labels:
  - bug
---

## What happened?

(describe the steps that led to the bug you encountered)

## What outcome was expected?

(what should have been the right outcome?)

## Screenshots

<details>
<summary>Screenshots</summary>

- (if you have screenshots, please paste them here. If not, you can delete from `<details>` to `</detail>`)

</details>

## Technical stuff (logs, etc)

<details>
<summary>Logs</summary>

```
(If you have logs to provide, please paste them here. If not, you can delete from `<details>` to `</detail>`)
```

</details>

