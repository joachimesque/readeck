---
name: Enhancement idea
about: When you want to share an idea to make Readeck Better 🤩
labels:
  - enhancement
---

(Describe your idea and how it would work. Screenshots are welcome if they help with your explanations.)