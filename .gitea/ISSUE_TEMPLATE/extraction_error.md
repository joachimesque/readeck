---
name: Extraction error
about: When a saved page is not well rendered in Readeck 😵
labels:
  - 'Extraction/Error'
---

**URL**: https://…

## What went wrong, what is missing?


## Screenshots

<details>
<summary>Screenshots</summary>

- (if you have screenshots, please paste them here. If not, you can delete from `<details>` to `</detail>`)

</details>
