module tools/doc

go 1.21

require (
	github.com/mangoumbrella/goldmark-figure v1.0.0
	github.com/yuin/goldmark v1.5.5
	github.com/yuin/goldmark-meta v1.1.0
)

require gopkg.in/yaml.v2 v2.3.0 // indirect
